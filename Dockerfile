# FROM sonar-scanner-image:latest AS sonarqube_scan
# # Here we are setting up a working directory to /app. It is like using `cd app` command
# WORKDIR /app
# # Copying all files from the project directory to our current location (/app) in image
# # except patterns mention in .dockerignore
# COPY . .
# # Execution of example command. Here it is used to show a list of files and directories.
# # It will be useful in later exercises in this tutorial.
# RUN ls -list
# # To execute sonar-scanner we just need to run "sonar-scanner" in the image.
# # To pass Sonarqube parameter we need to add "-D"prefix to each as in the example below
# # sonar.host.url is property used to define URL of Sonarqube server
# # sonar.projectKey is used to define project key that will be used to distinguish it in
# # sonarqube server from other projects
# # sonar.sources directory for sources of project
# RUN sonar-scanner \
#   -Dsonar.projectKey=testScheduler \
#   -Dsonar.sources=. \
#   -Dsonar.host.url=http://192.168.160.96:9000 \
#   -Dsonar.login=54f9ef88da21b3b1a2015bd4dc6cf52589df87f0

FROM node:14
WORKDIR /app
#COPY package.json /app
RUN npm install
COPY . .
CMD node server.js
EXPOSE 8000
